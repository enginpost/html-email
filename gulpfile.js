//instantiate GULP node packages

var gulp = 				require('gulp'),
	imagemin = 			require('gulp-imagemin'),
	gutil = 			require('gulp-util'),
	pngquant =			require('imagemin-pngquant'),
	imageresize =		require('gulp-image-resize'),
	connect = 			require('gulp-connect'),
	minify = 			require('gulp-minify-inline'),
	rename =			require('gulp-rename'),
	gulpif =			require('gulp-if'),
	inject =			require('gulp-inject-file'),
	rebase = 			require('rebase/tasks/gulp-rebase'),
	removeComments = 	require('gulp-remove-html-comments'),
	ftp = 				require('vinyl-ftp'),
	mail =				require('gulp-mail'),
	gulpsequence =		require('gulp-sequence'),
	env = "";

//Configuration: start

	//FTP configuration
var smtpUsername = 			"steve.mcdonald@enginpost.com",
    smtpPassword = 			"W1ldw00d!",
    smtpHost =				"mail.enginpost.com",
    smtpPort =				"25",
    smtpFrom =				"GULP <steve.mcdonald@enginpost.com>",
    smtpSubject =			"HTML email test",
    smtpTo =				[ "steve.mcdonald@navitus.com" ];

	//SMTP Email configuration
var emailWebURL = 			"http://enginpost.com/email/html/test",
    ftpHost =				"ftp.enginpost.com"
    ftpUsername = 			"gulp@enginpost.com",
    ftpPassword = 			"Gulpgulp!!",
    ftpParallel =			"10";

	//Design configuration
var imagesFolder = 			"images";

	//QA configuration
var imageMaxHeight =		"150",
	pngQuality =			"85-90",
	pngCompressionSpeed =	"4";

//Configuration: end



//read modes
env = process.env.NODE_ENV || "design";
gutil.log("environment: " + env);
//env: design, qa
var smtpInfo = {
	auth: {
		user: smtpUsername,
		pass: smtpPassword
	},
	host: smtpHost,
	secureConnection: false,
	port: smtpPort
};

var ftpInfo = {
	host: ftpHost,
	user: ftpUsername,
	password: ftpPassword,
	parallel: ftpParallel,
	log: gutil.log
};
var globs = [ 'builds/qa/*' ];
var ftpConnection = ftp.create( ftpInfo );

gulp.task('images', function(){
	var imagePath = 'builds/design/' + imagesFolder + '/*';
	gulp.src( imagePath )
		.pipe(imageresize({height:imageMaxHeight,upscale:false}))
		.pipe(imagemin({
			progressive: true,
			svgoPlugins: [{removeViewbox: false}],
			use: [pngquant({ quality: pngQuality, speed: pngCompressionSpeed })]
		}))
		.pipe(gulp.dest('builds/qa'));
});

gulp.task('rebaseImages', function(){
	var rebaseObj = {};
	rebaseObj.img = {};
	if( env == "design"){
		rebaseObj.img[imagesFolder] = '.';
	}else{
		rebaseObj.img[imagesFolder] = emailWebURL;
	}
	gutil.log("rebase should be " + rebaseObj.img.images );
	gulp.src( 'builds/design/*.html' )
		.pipe( rebase( rebaseObj ) );
});

gulp.task('html', function(){
	gulp.src( 'builds/design/*.html' )
		.pipe(removeComments())
		.pipe(inject({pattern: '<inject file="<filename>">'}))
		.pipe(gulpif(env === 'qa', minify()))
		.pipe(gulp.dest('builds/qa'));
});

gulp.task('watch', function(){
	gulp.watch('builds/design/css/*.css',['sequence' + env + 'Html']);
	gulp.watch('builds/design/index.html',['sequence' + env + 'Html']);
	gulp.watch('builds/design/' + imagesFolder + '/*',['sequence' + env + 'HtmlAndImages']);
});

gulp.task('connect', function(){
	if( env === "design"){
		connect.server({
			root: 'builds/qa',
			livereload: true
		});
	}
});
gulp.task('refreshConnection', function(){
	connect.reload();
});
gulp.task('sendmessage', function(){
	gulp.src( globs, {buffer: false} )
		.pipe( ftpConnection.dest('.') );
	gulp.src('builds/qa/index.html')
		.pipe(mail({
			subject: smtpSubject,
			to: smtpTo,
			from: smtpFrom,
			smtp: smtpInfo
		}));
});

gulp.task('env', function(){
	gutil.log("The NODE_ENV is set to " + env);
});

gulp.task('sequencedesignHtml',gulpsequence('rebaseImages','html','refreshConnection'));
gulp.task('sequencedesignHtmlAndImages',gulpsequence('images','rebaseImages','html','refreshConnection'));
gulp.task('sequenceqaHtml',gulpsequence('rebaseImages','html','sendmessage'));
gulp.task('sequenceqaHtmlAndImages',gulpsequence('images','rebaseImages','html','sendmessage'));

gulp.task('default', ['images','rebaseImages','html','watch','connect']);